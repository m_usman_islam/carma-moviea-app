import { createStore } from 'vuex';

//importing moudles
import movies from './modules/movies';

export const store = createStore({
  modules: { movies },
});
