import { actions } from './actions';
import { mutations } from './mutations';
import { getters } from './getters';

import { LOADING_STATE_STATUS } from '@/helpers/contants';

import { reactive } from '@vue/reactivity';

const state = reactive({
  upComingmovies: null,
  upComingloading: LOADING_STATE_STATUS.INIT,

  topRatedMovies: null,
  topRatedMoviesloading: LOADING_STATE_STATUS.INIT,

  populorMovies: null,
  populorMoviesloading: LOADING_STATE_STATUS.INIT,

  recommendationMovies: null,
  recommendationMoviesloading: LOADING_STATE_STATUS.INIT,

  singleMovie: null,
  singleMovieloading: LOADING_STATE_STATUS.INIT,

  searchMovies: null,
  searchMoviesloading: LOADING_STATE_STATUS.INIT,

  toggleSearch: false,
});

const movies = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};

export default movies;
