export const mutations = {
  //loading status
  LOADING_UPCOMING_STATUS(state, payload) {
    state.upComingloading = payload;
  },
  LOADING_TOPRATED_MOVIES(state, payload) {
    state.topRatedMoviesloading = payload;
  },
  LOADING_POPULOR_MOVIES(state, payload) {
    state.populorMoviesloading = payload;
  },
  LOADING_SINGLE_MOVIE(state, payload) {
    state.singleMovieloading = payload;
  },
  LOADING_RECOMMENDATOINS_MOVIES(state, payload) {
    state.recommendationMoviesloading = payload;
  },
  LOADING_SEARCH_MOVIES(state, payload) {
    state.searchMoviesloading = payload;
  },

  //states
  SET_ALL_UPCOMING_MOVIES(state, payload) {
    state.upComingmovies = payload;
  },
  SET_ALL_TOPRATED_MOVIES(state, payload) {
    state.topRatedMovies = payload;
  },
  SET_ALL_POPULOR_MOVIES(state, payload) {
    state.populorMovies = payload;
  },
  SET_ALL_SINGLE_MOVIE(state, payload) {
    state.singleMovie = payload;
  },

  SET_ALL_RECOMMENDATOINS_MOVIES(state, payload) {
    state.recommendationMovies = payload;
  },
  SET_ALL_SEARCH_MOVIES(state, payload) {
    state.searchMovies = payload;
  },
  resetSearchMovies(state, payload) {
    state.searchMovies = null;
  },
  updateToogle(state, payload) {
    state.toggleSearch = !payload;
  },
};
