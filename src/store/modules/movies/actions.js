import { axios } from '@/config/axios';
import {
  upcoming,
  topRated,
  populor,
  movieDetails,
  moviesRecomendation,
  searchMovies,
} from '@/endpoints/';

import { LOADING_STATE_STATUS } from '@/helpers/contants.js';

export const actions = {
  async fetchUpComingMovies({ commit }) {
    try {
      commit('LOADING_UPCOMING_STATUS', LOADING_STATE_STATUS.PENDING);
      const response = await axios.get(upcoming);

      commit('SET_ALL_UPCOMING_MOVIES', response.data.results);
      commit('LOADING_UPCOMING_STATUS', LOADING_STATE_STATUS.READY);
    } catch (e) {
      console.log('error', e);
      commit('LOADING_UPCOMING_STATUS', LOADING_STATE_STATUS.ERROR);
    }
  },
  async fetchTopRatedMovies({ commit }) {
    try {
      commit('LOADING_TOPRATED_MOVIES', LOADING_STATE_STATUS.PENDING);
      const response = await axios.get(topRated);

      commit('SET_ALL_TOPRATED_MOVIES', response.data.results);
      commit('LOADING_TOPRATED_MOVIES', LOADING_STATE_STATUS.READY);
    } catch (e) {
      console.log('error', e);
      commit('LOADING_TOPRATED_MOVIES', LOADING_STATE_STATUS.ERROR);
    }
  },
  async fetchPopulorMovies({ commit }) {
    try {
      commit('LOADING_UPCOMING_STATUS', LOADING_STATE_STATUS.PENDING);
      const response = await axios.get(populor);

      commit('SET_ALL_POPULOR_MOVIES', response.data.results);
      commit('LOADING_UPCOMING_STATUS', LOADING_STATE_STATUS.READY);
    } catch (e) {
      console.log('error', e);
      commit('LOADING_UPCOMING_STATUS', LOADING_STATE_STATUS.ERROR);
    }
  },
  async fetchSingleMovie({ commit }, payload) {
    try {
      commit('LOADING_SINGLE_MOVIE', LOADING_STATE_STATUS.PENDING);

      const response = await axios.get(movieDetails(payload));

      commit('SET_ALL_SINGLE_MOVIE', response.data);
      commit('LOADING_SINGLE_MOVIE', LOADING_STATE_STATUS.READY);
    } catch (e) {
      console.log('error', e);
      commit('LOADING_SINGLE_MOVIE', LOADING_STATE_STATUS.ERROR);
    }
  },
  async fetchRecomendationMovies({ commit }, payload) {
    try {
      commit('LOADING_RECOMMENDATOINS_MOVIES', LOADING_STATE_STATUS.PENDING);

      const response = await axios.get(moviesRecomendation(payload));

      commit('SET_ALL_RECOMMENDATOINS_MOVIES', response.data.results);
      commit('LOADING_RECOMMENDATOINS_MOVIES', LOADING_STATE_STATUS.READY);
    } catch (e) {
      console.log('error', e);
      commit('LOADING_RECOMMENDATOINS_MOVIES', LOADING_STATE_STATUS.ERROR);
    }
  },
  async fetchSearchMovies({ commit }, payload) {
    try {
      commit('LOADING_SEARCH_MOVIES', LOADING_STATE_STATUS.PENDING);

      const response = await axios.get(searchMovies(payload));

      commit('SET_ALL_SEARCH_MOVIES', response.data.results);
      commit('LOADING_SEARCH_MOVIES', LOADING_STATE_STATUS.READY);
    } catch (e) {
      console.log('error', e);
      commit('LOADING_SEARCH_MOVIES', LOADING_STATE_STATUS.ERROR);
    }
  },
  resetSearchMovies({ commit }) {
    commit('resetSearchMovies');
  },
  updateToogle({ commit }, payload) {
    commit('updateToogle', payload);
  },
};
