import { store } from '@/store';
import { router } from '@/router';
import Notifications from '@kyvg/vue3-notification';
export const plugins = [store, router, Notifications];
