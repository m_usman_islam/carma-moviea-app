import config from './index';
import instance from 'axios';
export const axios = instance.create({
  baseURL: config.baseURL,
});
