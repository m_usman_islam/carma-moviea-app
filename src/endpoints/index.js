let api_key = 'ee5d84b9dcaef7015d6ac675db148174';

export const upcoming = `/movie/upcoming?api_key=${api_key}&language=en-US`;
export const topRated = `/movie/top_rated?api_key=${api_key}&language=en-US`;
export const populor = `/movie/popular?api_key=${api_key}&language=en-US`;

//get

export const movieDetails = (slug) => {
  return `/movie/${slug}?api_key=${api_key}&language=en-US`;
};
export const moviesRecomendation = (slug) => {
  return `/movie/${slug}/recommendations?api_key=${api_key}&language=en-US`;
};
export const searchMovies = (query) => {
  return `/search/movie?api_key=${api_key}&query=${query}&language=en-US`;
};
