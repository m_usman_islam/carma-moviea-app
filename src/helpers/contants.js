export const LOADING_STATE_STATUS = {
  READY: 'ready',
  PENDING: 'pending',
  ERROR: 'error',
  INIT: 'init',
  DRAFT: 'draft',
  CONNECTED: 'connected',
};
