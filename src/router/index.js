import { createRouter, createWebHistory } from 'vue-router';

const Index = () => import('../pages/index.vue');
const MovieDetails = () => import('../pages/movieDetails.vue');
const MoviesStatictis = () => import('../pages/moviesStatictis.vue');

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index,
  },
  {
    path: '/movie-details/:id',
    name: 'MovieDetails',
    component: MovieDetails,
  },
  {
    path: '/top-ratetd-movie-stats',
    name: 'MoviesStatictis',
    component: MoviesStatictis,
  },
];
export const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return { top: 0 };
  },
});
