import { createApp } from 'vue';
import { plugins } from './plugins/index';
import '@/assets/scss/style.scss';
import App from './App.vue';

const app = createApp(App);

plugins.map((plugin) => {
  app.use(plugin);
});

app.mount('#app');
